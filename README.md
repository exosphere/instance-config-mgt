This repository contains configuration management code that Exosphere uses to deploy a new instance. Currently, it installs and configures Docker and Apache Guacamole server.

To run the code manually on a cloud instance, do this:
```
virtualenv /opt/ansible-venv
. /opt/ansible-venv/bin/activate
pip install ansible-base
PASSPHRASE="enter-exouser-passphrase-here"
ansible-pull --url https://gitlab.com/exosphere/instance-config-mgt.git --directory /opt/instance-config-mgt -i /opt/instance-config-mgt/ansible/hosts /opt/instance-config-mgt/ansible/playbook.yml
```

Optionally, pass the `--checkout` argument to specify a git tag or commit hash.

For now, we are using only [built-in modules](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/#modules) because Exosphere uses the `ansible-base` / `ansible-core` package.

## Configuring instance deployment

Set these variables and pass them to Ansible.

| variable     | type    | required | description                                                             |
|--------------|---------|----------|-------------------------------------------------------------------------|
| PASSPHRASE   | string  | yes      | passphrase for local user. set as environment variable, not Ansible var |
| gui_enabled  | boolean | no       | deploys VNC server, configures Guacamole to serve graphical desktop     |